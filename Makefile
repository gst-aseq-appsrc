CFLAGS = -std=c99 -pedantic -pedantic-errors -Wall -g3 -O2
CFLAGS += -fno-common \
	-fvisibility=hidden \
	-Wall \
	-Wdeclaration-after-statement \
	-Wextra \
	-Wformat=2 \
	-Winit-self \
	-Winline \
	-Wpacked \
	-Wpointer-arith \
	-Wlarger-than-65500 \
	-Wmissing-declarations \
	-Wmissing-format-attribute \
	-Wmissing-noreturn \
	-Wmissing-prototypes \
	-Wnested-externs \
	-Wold-style-definition \
	-Wredundant-decls \
	-Wsign-compare \
	-Wstrict-aliasing=2 \
	-Wstrict-prototypes \
	-Wundef \
	-Wunreachable-code \
	-Wunsafe-loop-optimizations \
	-Wunused-but-set-variable \
	-Wno-unused-parameter \
	-Wwrite-strings \
	-Wp,-D_FORTIFY_SOURCE=2 \
	-fstack-protector \
	--param=ssp-buffer-size=4

# for strdup
CFLAGS += -D_POSIX_C_SOURCE=200809L

CFLAGS += $(shell pkg-config --cflags gstreamer-1.0 gstreamer-app-1.0 alsa)
LDLIBS := $(shell pkg-config --libs gstreamer-1.0 gstreamer-app-1.0 alsa)

gst-aseq-appsrc: gst-aseq-appsrc.o

clean:
	rm -f *.o *~ gst-aseq-appsrc
